package org.lorainelab.igb.context.menu.api.quickstart;

import aQute.bnd.annotation.component.Component;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.lorainelab.igb.menu.api.AnnotationContextMenuProvider;
import org.lorainelab.igb.menu.api.model.AnnotationContextEvent;
import org.lorainelab.igb.menu.api.model.ContextMenuItem;
import org.lorainelab.igb.menu.api.model.ContextMenuSection;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Component(immediate = true)
public class ContextMenuExtension implements AnnotationContextMenuProvider {

    private static final Logger LOG = LoggerFactory.getLogger(ContextMenuExtension.class);
    private static final String ICONPATH = "terminal.png";

    @Override
    public Optional<List<ContextMenuItem>> buildMenuItem(AnnotationContextEvent event) {
        ContextMenuItem contextMenuItem = new ContextMenuItem("Log Selection ids", (Void t) -> {
            event.getSelectedItems().stream().map(selectedSym -> selectedSym.getID()).forEach(LOG::info);
            return t;
        });
        try (InputStream resourceAsStream = ContextMenuExtension.class.getClassLoader().getResourceAsStream(ICONPATH)) {
            contextMenuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        contextMenuItem.setMenuSection(ContextMenuSection.INFORMATION);
        return Optional.ofNullable(Arrays.asList(contextMenuItem));
    }

}
